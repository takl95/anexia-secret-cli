#!/usr/bin/env node

const { Command } = require('commander')
const clipboardy = require('clipboardy')
const chalk = require('chalk')
const request = require('request-promise-native')
const getInput = require('node-piped').getInput

;(async () => {
  const program = new Command()
  program
    .option(
      '-c, --clipboard',
      'Extract sensitive data from clipboard and output the secret url to the clipboard.',
      false
    )
    .option(
      '-e, --expire <expire>',
      'Time to expire. Example: ' + chalk.yellow('+1 hour')
    )
    .option(
      '-p, --password <password>',
      'Additional password to secure the secret with'
    )
    .action(async ({ clipboard, expire, password }) => {
      if (process.stdin.isTTY && !clipboard) {
        console.log(chalk.red('Secret was not provided via pipe.'))
        process.exit()
      }
      let secret = null
      if (clipboard) {
        secret = clipboardy.readSync()
      }
      if (!secret) {
        secret = await getInput()
      }
      if (!secret.length) {
        console.log(chalk.red('Secret was not provided.'))
        process.exit()
      }

      let { url } = await request({
        method: 'POST',
        url: 'https://secret.anexia.com/api/secret',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: {
          secret,
          password,
          expire,
        },
        json: true,
      }).catch((error) => console.log(chalk.red(error)))

      url = url.replace(/^/, 'https://secret.anexia.com/secret/')

      if (clipboard) {
        clipboardy.writeSync(url)
        console.log(chalk.green('Secret URL copied to clipboard'))
        return
      }
      console.log(url)
    })

  await program.parseAsync(process.argv)
})()
