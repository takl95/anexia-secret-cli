<h3 align="center">anexia-secret-cli</h3>
  <p align="center">
   A CLI wrapper around the REST Api of secret.anexia.com to send sensitive data securely.
  </p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

A CLI command written in NodeJS.

A CLI wrapper around the REST Api of secret.anexia.com to send sensitive data securely.

## Getting Started

To get the script up and running follow these steps.

### Prerequisites

This is a list of things you need to use the software.

* NodeJS

### Installation

  ```sh
  npm i -g anexia-secret-cli
  ```

## Usage
```sh
Usage: secret [options]

Options:
-c, --clipboard Extract sensitive data from clipboard and output the secret url to the clipboard. (default: false)
-e, --expire <expire>      Time to expire. Example: +1 hour
-p, --password <password>  Additional password to secure the secret with -h, --help display help for command
```

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request / Pull Request



## License
Distributed under the MIT License. See `LICENSE.txt` for more information.



## Contact

Laszlo Takacs - laszlotakacs.95@gmail.com 
